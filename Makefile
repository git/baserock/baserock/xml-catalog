INSTALL := install

DESTDIR :=

SYSCONFDIR := etc
DATADIR := usr/share

ROOTXMLDIR := ${SYSCONFDIR}/xml
LOCALXMLDIR := ${DATADIR}/xml
COREPACKAGEDIR := ${LOCALXMLDIR}/xml-core
XHTMLPACKAGEDIR := ${LOCALXMLDIR}/xml-xhtml1

install: amend-catalogs

amend-catalogs: install-files

install-files:
	${INSTALL} -d ${DESTDIR}/${ROOTXMLDIR}
	${INSTALL} -m 644 catalog ${DESTDIR}/${ROOTXMLDIR}
	${INSTALL} -m 644 xml-core.xml ${DESTDIR}/${ROOTXMLDIR}
	${INSTALL} -d ${DESTDIR}/${COREPACKAGEDIR}	
	${INSTALL} -m 644 xml-core/catalog.dtd ${DESTDIR}/${COREPACKAGEDIR}
	${INSTALL} -m 644 xml-core/catalog.xml ${DESTDIR}/${COREPACKAGEDIR}
	${INSTALL} -d ${DESTDIR}/${XHTMLPACKAGEDIR}	
	${INSTALL} -m 644 xml-xhtml1.xml ${DESTDIR}/${ROOTXMLDIR}
	${INSTALL} -m 644 xml-xhtml1/catalog.xml ${DESTDIR}/${XHTMLPACKAGEDIR}
	${INSTALL} -m 644 xhtml1-20020801/DTD/xhtml-lat1.ent ${DESTDIR}/${XHTMLPACKAGEDIR}
	${INSTALL} -m 644 xhtml1-20020801/DTD/xhtml-symbol.ent ${DESTDIR}/${XHTMLPACKAGEDIR}
	${INSTALL} -m 644 xhtml1-20020801/DTD/xhtml-special.ent ${DESTDIR}/${XHTMLPACKAGEDIR}
	${INSTALL} -m 644 xhtml1-20020801/DTD/xhtml1-strict.dtd ${DESTDIR}/${XHTMLPACKAGEDIR}
	${INSTALL} -m 644 xhtml1-20020801/DTD/xhtml1-transitional.dtd ${DESTDIR}/${XHTMLPACKAGEDIR}
	${INSTALL} -m 644 xhtml1-20020801/DTD/xhtml1-frameset.dtd ${DESTDIR}/${XHTMLPACKAGEDIR}

amend-catalogs:
	xmlcatalog --noout --add delegatePublic \
		"-//OASIS//DTD XML Catalogs V1.0//EN" "file:///${COREPACKAGEDIR}/catalog.xml" \
		${DESTDIR}/${ROOTXMLDIR}/xml-core.xml
	xmlcatalog --noout --add delegateSystem \
		"http://www.oasis-open.org/committees/entity/release/1.0/catalog.dtd" "file:///${COREPACKAGEDIR}/catalog.xml" \
		${DESTDIR}/${ROOTXMLDIR}/xml-core.xml
	xmlcatalog --noout --add delegatePublic \
		"-//W3C//DTD XHTML 1.0" "file:///${XHTMLPACKAGEDIR}/catalog.xml" \
		${DESTDIR}/${ROOTXMLDIR}/xml-xhtml1.xml
	xmlcatalog --noout --add delegatePublic \
		"-//W3C//ENTITIES Latin 1 for XHTML//EN" "file:///${XHTMLPACKAGEDIR}/catalog.xml" \
		${DESTDIR}/${ROOTXMLDIR}/xml-xhtml1.xml
	xmlcatalog --noout --add delegatePublic \
		"-//W3C//ENTITIES Symbols for XHTML//EN" "file:///${XHTMLPACKAGEDIR}/catalog.xml" \
		${DESTDIR}/${ROOTXMLDIR}/xml-xhtml1.xml
	xmlcatalog --noout --add delegatePublic \
		"-//W3C//ENTITIES Special for XHTML//EN" "file:///${XHTMLPACKAGEDIR}/catalog.xml" \
		${DESTDIR}/${ROOTXMLDIR}/xml-xhtml1.xml
	xmlcatalog --noout --add delegateSystem \
		"http://www.w3.org/TR/xhtml1/DTD" "file:///${XHTMLPACKAGEDIR}/catalog.xml" \
		${DESTDIR}/${ROOTXMLDIR}/xml-xhtml1.xml
