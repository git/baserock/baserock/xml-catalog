#!/bin/sh

set -e

if [ -z "$SYSCONFDIR" ]; then
    SYSCONFDIR=etc
fi  

if [ -z "$DESTDIR" ]; then
    DESTDIR=
fi

ROOTXMLDIR=$SYSCONFDIR/xml

xmlcatalog --noout --add delegatePublic \
	"-//OASIS//DTD XML Catalogs V1.0//EN" "file:///$ROOTXMLDIR/xml-core.xml" \
	$DESTDIR/$ROOTXMLDIR/catalog
xmlcatalog --noout --add delegateSystem \
	"http://www.oasis-open.org/committees/entity/release/1.0/catalog.dtd" "file:///$ROOTXMLDIR/xml-core.xml" \
	$DESTDIR/$ROOTXMLDIR/catalog
xmlcatalog --noout --add delegatePublic \
	"-//W3C//DTD XHTML 1.0" "file:///$ROOTXMLDIR/xml-xhtml1.xml" \
	$DESTDIR/$ROOTXMLDIR/catalog
xmlcatalog --noout --add delegatePublic \
	"-//W3C//ENTITIES Latin 1 for XHTML//EN" "file:///$ROOTXMLDIR/xml-xhtml1.xml" \
	$DESTDIR/$ROOTXMLDIR/catalog
xmlcatalog --noout --add delegatePublic \
	"-//W3C//ENTITIES Symbols for XHTML//EN" "file:///$ROOTXMLDIR/xml-xhtml1.xml" \
	$DESTDIR/$ROOTXMLDIR/catalog
xmlcatalog --noout --add delegatePublic \
	"-//W3C//ENTITIES Special for XHTML//EN" "file:///$ROOTXMLDIR/xml-xhtml1.xml" \
	$DESTDIR/$ROOTXMLDIR/catalog
xmlcatalog --noout --add delegateSystem \
	"http://www.w3.org/TR/xhtml1/DTD" "file:///$ROOTXMLDIR/xml-xhtml1.xml" \
	$DESTDIR/$ROOTXMLDIR/catalog
